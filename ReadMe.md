
# gosc

Pure Go [http://golang.org](http://golang.org) support for OSC [http://opensoundcontrol.org](http://opensoundcontrol.org)

Docs are available at [http://godoc.org/bitbucket.org/liamstask/gosc](http://godoc.org/bitbucket.org/liamstask/gosc)

[![Build Status](https://drone.io/bitbucket.org/liamstask/gosc/status.png)](https://drone.io/bitbucket.org/liamstask/gosc/latest)

## Install

`go get bitbucket.org/liamstask/gosc`


## Client Example
    :::go
    import (
	    "bitbucket.org/liamstask/gosc"
        "net"
    )

    m := &osc.Message{Address: "/my/message"}
    m.Args = append(m.Args, int32(12345))
    m.Args = append(m.Args, "important")

    // error checking omitted for brevity...
	addr, _ := net.ResolveUDPAddr("udp", "127.0.0.1:12000")
	conn, _ := net.DialUDP("udp", nil, addr)

	numbytes, err := m.WriteTo(conn)
	if err != nil {
		// handle error
	}


## Server Example
	:::go
	import (
		"bitbucket.org/liamstask/gosc"
		"fmt"
	)

	osc.HandleFunc("/msg/address", func(msg *osc.Message) {
		fmt.Printf("Receieved message: " + msg.Address + "[")

		for _, arg := range msg.Args {
			switch v := arg.(type) {
			default:
				fmt.Printf("unknown argument type")
			case float32, float64:
				fmt.Printf("%f\n", v)
			}
		}

		fmt.Printf("]\n")
	})

	err := osc.ListenAndServeUDP(":8000", nil)

	if err != nil {
		// handle error.
	}


See the tests for more detailed usage.

# Contributors

Thank you!

* Kelly Dunn (kellydunn)
* Clinton Freeman (clinton_freeman)
